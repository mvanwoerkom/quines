% encode.erl -- list program text of quine.erl as Erlang strings
-module(encode).
-export([main/0]).

main() ->
  Filename = "quine.erl",
  case file:read_file(Filename) of
    {error, Reason} ->
      io:format("error: ~p~n", [Reason]);
    {ok, Binary} ->
      [_, Code] = binary:split(Binary, <<"],\n">>),  % ignore header and data sections
      L = binary:split(Code, <<"\n">>, [global]),
      print(L)
  end.


print([]) ->
  true;
print([H|T]) ->
  L = binary_to_list(H),
  case H of
    <<>> ->
      io:format("     \"\",~n");
    _ ->
      io:format("     ~p,~n", [L])  % ~p does the job, except for empty strings
  end,
  print(T).
