% quine.erl -- an Erlang quine
% mvw, 14.06.2021

-module(quine).
-export([main/0]).

main() ->
  Data =
    [
     "  % this line and the rest has to be put into the data definition above",
     "  % you can use the encode program to generate the data lines",
     "  header(),",
     "  copy_data(Data),",
     "  list_code(Data),",
     "  true.",
     "",
     "",
     "% start section of the program",
     "header() ->",
     "  io:format(\"% quine.erl -- an Erlang quine~n\"),",
     "  io:format(\"% mvw, 14.06.2021~n\"),",
     "  io:format(\"~n\"),",
     "  io:format(\"-module(quine).~n\"),",
     "  io:format(\"-export([main/0]).~n\"),",
     "  io:format(\"~n\"),",
     "  io:format(\"main() ->~n\").",
     "",
     "",
     "%  repeat the data section",
     "copy_data(Data) ->",
     "  io:format(\"  Data =~n\"),",
     "  io:format(\"    [~n\"),",
     "  copy_data_helper(Data),",
     "  io:format(\"    ],~n\").",
     "",
     "copy_data_helper([]) ->",
     "  true;",
     "copy_data_helper([H|T]) ->",
     "  case H of",
     "    [] ->",
     "      io:format(\"     \\\"\\\"\");",
     "    _ ->",
     "      io:format(\"     ~p\", [H])  % the ~p does the proper escaping",
     "  end,",
     "  case T of",
     "    [] ->",
     "      io:format(\"~n\");  % last line",
     "    _ ->",
     "      io:format(\",~n\")",
     "  end,",
     "  copy_data_helper(T).",
     "",
     "",
     "% list the code within the data to reproduce the final section",
     "list_code([]) ->",
     "  true;",
     "list_code([H|T]) ->",
     "  io:format(\"~s~n\", [H]),  % ~s does plain string output",
     "  list_code(T)."
    ],
  % this line and the rest has to be put into the data definition above
  % you can use the encode program to generate the data lines
  header(),
  copy_data(Data),
  list_code(Data),
  true.


% start section of the program
header() ->
  io:format("% quine.erl -- an Erlang quine~n"),
  io:format("% mvw, 14.06.2021~n"),
  io:format("~n"),
  io:format("-module(quine).~n"),
  io:format("-export([main/0]).~n"),
  io:format("~n"),
  io:format("main() ->~n").


%  repeat the data section
copy_data(Data) ->
  io:format("  Data =~n"),
  io:format("    [~n"),
  copy_data_helper(Data),
  io:format("    ],~n").

copy_data_helper([]) ->
  true;
copy_data_helper([H|T]) ->
  case H of
    [] ->
      io:format("     \"\"");
    _ ->
      io:format("     ~p", [H])  % the ~p does the proper escaping
  end,
  case T of
    [] ->
      io:format("~n");  % last line
    _ ->
      io:format(",~n")
  end,
  copy_data_helper(T).


% list the code within the data to reproduce the final section
list_code([]) ->
  true;
list_code([H|T]) ->
  io:format("~s~n", [H]),  % ~s does plain string output
  list_code(T).
