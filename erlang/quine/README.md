For some background see http://www.madore.org/~david/computers/quine.html

Usage:
- write the code for quine.erl
- run the encode program (run-encode.cmd)
- insert the output from encode into the definition of the Data block in quine.erl
- remove the last empty line, remove the last comma
- run the quine program (run-quine.cmd)
- check differences between source code and output (delta.cmd)
